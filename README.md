# migrate_mysql_to_postgres

#Подготовка к posgresql timescaledb
```
apt-get install php7.2-pgsql
echo "deb http://apt.postgresql.org/pub/repos/apt/ bionic-pgdg main" > /etc/apt/sources.list.d/pgdg.list
curl --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -
apt-get update
apt-get install postgresql-12
add-apt-repository ppa:timescale/timescaledb-ppa
apt-get update
apt-get install timescaledb-postgresql-12
timescaledb-tune
systemctl restart postgresql
sudo -u postgres createuser --pwprompt zabbix
sudo -u postgres createdb -O zabbix zabbix
```
#миграция
```
apt-get install sbcl unzip libsqlite3-dev make curl gawk freetds-dev libzip-dev
git clone https://github.com/dimitri/pgloader.git
cd /path/to/pgloader
make pgloader
./build/bin/pgloader zabbix.load
```

```
#zcat /usr/share/doc/zabbix-server-pgsql*/create.sql.gz | sudo -u zabbix psql zabbix 
#echo "shared_preload_libraries = 'timescaledb'" >> /etc/postgresql/12/main/postgresql.conf
#echo "CREATE EXTENSION IF NOT EXISTS timescaledb CASCADE;" | sudo -u postgres psql zabbix
#cat /usr/share/doc/zabbix-server-pgsql*/timescaledb.sql | sudo -u postgres psql zabbix
```

https://sysadmins.ws/viewtopic.php?f=31&t=699
